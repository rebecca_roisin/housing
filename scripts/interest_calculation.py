import csv
import datetime
from dateutil.relativedelta import relativedelta
import dateutil.parser as dateparser
import matplotlib.pyplot as plt
import itertools
import numpy as np
from datetime import timedelta
# from numpy.linalg import lstsq

def dat_group(x):
    return x[8]

def make(datas, max_months, min_months):
    #datas = [d for d in datas if d[3][:3] == fil]
    datas.sort(key = dat_group)

    sales_record = {}
    d = []
    labs = []
    Zall = None
    Call = None
    ri = np.ones((1, max_months - min_months + 1)) * 0.01
    for i, (k, g) in enumerate(itertools.groupby(datas, dat_group)):
        recs = list(g)
        sales = []
        for rec in recs:
            d_sale = rec[2]
            sales.append((dateparser.parse(d_sale), int(rec[1])))
        recs.sort(key=lambda x: x[2])
        sales_record[k] = sales

        if len(recs) < 2: 
            continue

        for i in range(len(recs) - 1):
            D1 = dateparser.parse(recs[i][2])
            m1 = D1.year*4 + (D1.month / 4) - min_months
            #m1 = D1.year * 12 + (D1.month) - min_months
            # m1 = D1.year - min_months
            D2 = dateparser.parse(recs[i+1][2])
            m2 = D2.year*4 + (D2.month / 4) - min_months
            # m2 = D2.year * 12 + (D2.month) - min_months
            # m2 = D2.year - min_months
            Z = np.zeros((1, max_months - min_months + 1 + 1))
            Z[0, m1:(m2 + 1)] = 1
            Z[0, -1] = m2 - m1 + 1
            if Zall is None:
                Zall = Z
            else:
                Zall = np.vstack([Zall, Z])
            
            C = np.array([[np.log(float(recs[i+1][1]) / float(recs[i][1]))]])
            if Call is None:
                Call = C
            else:
                Call = np.vstack([Call, C])

    return sales_record, Zall, Call

def main():
    datafiles = ["../data/n17.csv", "../data/n15.csv"]

    for en, datafile in enumerate(datafiles):
        datasx = []

        min_date = None
        max_date = None

        #with open("..\Documents\python_housing\All_trsansactions.csv", "r") as f:
        #   transactions = csv.reader(f, delimiter=",")
        #   for row in transactions:
        with open(datafile, "r") as f:
            transactions = csv.reader(f, delimiter=",")
            for row in transactions:

                #if row[3].startswith("17 "):
                datasx += [row]

        dates = [dateparser.parse(d[2]) for d in datasx]
        min_date = min(dates)
        min_months = min_date.year * 4 + min_date.month / 4
        # min_months = min_date.year * 12 + min_date.month
        # min_months = min_date.year


        max_date = max(dates)
        max_months = max_date.year * 4 + max_date.month / 4
        # max_months = max_date.year * 12 + max_date.month 
        # max_months = max_date.year


        labels = []
        d = min_date
        while d < max_date:
            labels += ["%s Q%s" % (d.year, 1 + d.month / 4)]
            d = d + relativedelta(months=3)


        bradshaw_sales, Zall, Call = make(datasx, max_months, min_months)
        num_cols = len(bradshaw_sales)

        # print Zall
        # print Call

        print np.version.version

        logOpRi, res, rank, sing = np.linalg.lstsq(Zall, Call)
        ri = np.expm1(logOpRi + logOpRi[-1])
        ri = ri[:-1]

        plt.plot(ri, label=en)
        xs = []
        ls = []
        for x,l in zip(range(len(labels)), labels):
            if x % 4 == 0:
                xs += [x]
                ls += [l]

    plt.legend()
    plt.xticks(xs, ls, rotation=45)
    plt.grid()
    plt.show()

if __name__ == "__main__":
    main()
import csv
import datetime
import matplotlib.pyplot as plt
import itertools
import numpy as np

def dat_group(x):
	y, m = int(x[2][:4]), int(x[2][5:7])
	return (y, m / 2)

def make(datas, fil, col="k"):
	datas = [d for d in datas if d[3][:3] == fil]
	datas.sort(key = lambda x: x[2])

	d = []
	labs = []
	for i, (k, g) in enumerate(itertools.groupby(datas, dat_group)):
		prices = sorted([int(gi[1]) for gi in g])
		lp = len(prices)
		Q1, med, Q3 = prices[lp/4], prices[lp/2], prices[3*lp/4]
		prices = [p for p in prices if med - 2 * (med - Q1) <= p <= med + 2 * (Q3 - med)]
		labs += [k]
		xmin, xmax = min(prices), max(prices)
		d += [[i, xmin, Q1, med, Q3, xmax]]

	d = np.array(d)
	x = d[:, 0]
	y = d[:, 3]
	plt.plot(x, y, color=col)
	plt.fill_between(x, d[:, 2], d[:, 4], color=col, alpha=0.1)
	plt.fill_between(x, d[:, 1], d[:, 5], color=col, alpha=0.1)

	ls = [(int(kx), kl[:4]) for i, (kx,kl) in enumerate(zip(x, labs)) if kl[1] == 0]
	xs = [k[0] for k in ls]
	labs = [k[1] for k in ls]


	plt.xticks(xs, labs, rotation=70)
	plt.yticks([i*100000 for i in range(20)])

	# inf = [y[0]]
	#for i in range(len(xs)):
	#	inf += [inf[-1] * 1.03]

	#plt.plot(x, inf[:-1], color="y")

def main():
	datas = []

	with open("../data/All_trsansactions.csv", "r") as f:
		transactions = csv.reader(f, delimiter=",")
		for row in transactions:
			datas += [row]

	plt.grid()
	make(datas, "N17", "k")
	make(datas, "N15", "r")
	make(datas, "E17", "b")
	make(datas, "E8 ", "g")
	make(datas, "N8 ", "c")
	make(datas, "SW2", "m")
	plt.legend(["N17", "N15", "E17", "E8", "N8", "SW2"])
	plt.savefig("../data/price_change.png")
	plt.show()

if __name__ == "__main__":
	main()
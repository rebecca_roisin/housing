import csv
import dateutil.parser as dateparser
import matplotlib.pyplot as plt
import itertools
import numpy as np

datasx = []

with open("../data/hackney_bramshaw.csv", "r") as f:
	transactions = csv.reader(f, delimiter=",")
	for row in transactions:
		datasx += [row]

def dat_group(x):
	return x[8]

def rate(Rec1, Rec2):
	D1 = Rec1[2]
	D2 = Rec2[2]
	months = float((12*int(D2[:4]) + int(D2[5:7])) - (12*int(D1[:4]) + int(D1[5:7])))
	M1 = float(Rec1[1])
	M2 = float(Rec2[1])
	r = (M2 / M1) ** (1.0 / months) - 1 
	return (months, r)

def make(datas):
	#datas = [d for d in datas if d[3][:3] == fil]
	datas.sort(key = dat_group)

	sales_record = {}
	d = []
	labs = []
	for i, (k, g) in enumerate(itertools.groupby(datas, dat_group)):
		recs = list(g)
		sales = []
		for rec in recs:
			d_sale = rec[2]
			sales.append((dateparser.parse(d_sale), int(rec[1])))
		recs.sort(key=lambda x: x[2])
		sales_record[k] = sales
	return sales_record

bradshaw_sales = make(datasx)
num_cols = len(bradshaw_sales)

cm = plt.get_cmap('gist_rainbow')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_color_cycle([cm(1.*i/num_cols) for i in range(num_cols)])

col = 0
all_X = []
all_Y = []
for k, v in bradshaw_sales.iteritems():
		color = cm(1.*col/num_cols)
		print k,v
		sale_times = []
		costs = []
		for t, cost in v:
			all_X.append(t)
			all_Y.append(cost)
			print t
			sale_times.append(t)
			costs.append(cost)
			plt.scatter(t, cost, c=color)
		plt.plot(sale_times, costs, c=color)
		col += 1

#plt.xticks(xs, labs, rotation=70)
plt.yticks([i*50000 for i in range(14)])
plt.grid()
tot_months = []
for i in all_X:
	num_months = (i.year * 12) + i.month
	tot_months.append(num_months) 

with open("../data/Bramshaw_months.csv", "w") as month_data:
	for i, j in zip(tot_months, all_Y):
		month_data.write("%s,%s\n" %(i,j))

#regr = linear_model.LinearRegression()
#regr.fit(tot_months, all_Y)
#plt.plot(all_X, regr.predict(all_X), color='blue',
#        linewidth=3)


#r i in range(NUM_COLORS):
#    color = cm(1.*i/NUM_COLORS)  # color will now be an RGBA tuple
#    ax.plot(np.arange(10)*(i+1))
#plt.legend(bradshaw_sales.keys())
plt.savefig("../data/Bramshaw_road_prices_fit.png")
plt.show()
plt.cla()
#plt.show()

import csv
import dateutil.parser as dateparser
import matplotlib.pyplot as plt
import itertools
import numpy as np

def make(datas, targets):
    
    # make function to sort / select data on 1st half of postcode:
    pc = lambda x: x[3].split(" ")[0] 
    #print datas[3]

    datas.sort(key = pc)

    sales_record = {}
    d = []
    labs = []
    for i, (k, g) in enumerate(itertools.groupby(datas, pc)):
        recs = list(g)
        sales = []
        for rec in recs:
            d_sale = rec[2]
            sales.append((dateparser.parse(d_sale), int(rec[1])))
        #recs.sort(key=lambda x: x[2])
        #for val in np.arange(1, 29, 1):

        # manually selecting postcodes -- very ugly
        for t in targets:
            if k.startswith(t): 
                sales_record[k] = sales
    return sales_record

def base100(x):
    base = x[0]
    y = [100*float(xi)/base for xi in x]
    return y

def main(datafile, imfile):

    # here, we are going to plot average transaction value and total transaction value
    # per region, per time period

    datasx = []
    cnt = 0


    # read data into datasx
    with open(datafile, "r") as f:
        transactions = csv.reader(f, delimiter=",")
        for row in transactions:
            datasx += [row]        
    print len(datasx)


    # get sales info for relevant regions
    regions = set(["N17", "N16", "E8", "SW9", "E17", "N15"])
    london_sales = make(datasx, regions)
    num_cols = len(london_sales)

    # set up colours
    cm = plt.get_cmap('gist_rainbow')
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_color_cycle([cm(1.*i/num_cols) for i in range(num_cols)])


    col = 0
    for k, sales in london_sales.iteritems():
        color = cm(1.*col/num_cols)
        #for rec in sales:
        #transactions = []

        # group by year of transaction
        tos = lambda x: (x[0].year)#, (x[0].month/4)*4)
        sales.sort(key=tos)
        date = []
        tot_sales = []
        av_sales = []
        for dats, trans in itertools.groupby(sales, tos):
            periods = dats #[0] #+ (dats[1] - 1) * (1.0 / 12)
            date.append(periods)
            trans_val = 0   # number of transactions
            no_trans = 0    # total value of all transactions
            for tr in trans:
                no_trans += 1
                trans_val += int(tr[1])

            if no_trans == 0:
                av_sales.append(0)
                print trans_val, no_trans
                print "None!"
            else:
                av_sales.append((float(trans_val)/no_trans))
                print trans_val, no_trans, float(trans_val) / no_trans
            tot_sales.append(trans_val)
        #plt.plot(date, base100(no_sales), c=color, label=k)
        plt.plot(date, tot_sales, c=color, label=k)
        #plt.plot(date, av_sales, c=color, label=k) 
        col += 1

    plt.xlim(1994, 2015)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    ax.legend(loc='upper left', bbox_to_anchor=(1, 1.0))

    plt.savefig(imfile)
    plt.show()

if __name__ == "__main__":
    data = "../data/north_south.csv"
    img = "../data/london_transactions_SE.pdf"
    main(data, img)

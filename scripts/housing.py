import csv
import sys



def main():
    all_transactions = sys.argv[1] # all transactions ever - from land registry
    postcode_file = sys.argv[2] # file containing subset of interesting postcodes
    my_file = sys.argv[3] # file to save relevant transactions


    # parse to get postcodes
    targets = set([])
    with open(postcode_file, "rb") as codes:
        postcodes = csv.reader(codes, dialect="excel", delimiter=",")
        for row in postcodes:
            print row[0]
            targets.add(str(row[0]))

    relevant_transactions = open(my_file, "w")

    # select relevant transactions and save
    with open(all_transactions, "r") as f:
        transactions = csv.reader(f, delimiter=",")
        for row in transactions:
            for t in targets:
                if row[3].startswith(t):
                    #if row[4] =="T" and row[6] == "F" and (row[3][:5] in targets or row[3][:6] in targets):
                    #if row[3] in targets:
                    relevant_transactions.write("%s\n" %(",".join(row)))
                    #print row[1], row[3]

    relevant_transactions.close()

if __name__ == "__main__":
    main()

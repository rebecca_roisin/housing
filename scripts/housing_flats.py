import csv

relevant_transactions = open("../data/north_london_flats.csv", "w")

targets = set(["N15", "N17", "E17", "E8 ", "N8 ", "SW2"])

with open("../data/pp-complete.csv", "r") as f:
	transactions = csv.reader(f, delimiter=",")
	for row in transactions:
		if row[4] =="F" and row[3][:3] in targets:
			relevant_transactions.write("%s\n" %(",".join(row)))
			# print row[1], row[3]

relevant_transactions.close()
		# print ", ".join(row)

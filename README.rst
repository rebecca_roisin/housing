Housing Scripts
***************
This is a set of scripts for analysis of property transactions from the Land Registry.

The code is currently messy and poorly documented, for which I apologise.

Here is a brief description of the data you need and the scripts included.


Getting the Data
*****************

You need two files:

The land registry has a massive csv file with all housing transactions since 1995 for the whole of the UK:

https://www.gov.uk/government/statistical-data-sets/price-paid-data-downloads#single-file

Some wonderful geek on the internet maintains a list of all postcodes in London. You can download them all as a .csv file here:
http://www.doogal.co.uk/london_postcodes.php

Save both these files in the "data" folder and you are mostly good to go.

Using the Scripts
*****************

The most important script is housing.py. Use it to parse out postcodes of interest from the main .csv file into a smaller one for fast processing.

Current usage: pp-complete.csv is the default name of the full dataset from the land registry; london_postcodes is a short csv file containing N17 postcodes, little_csv is the name of your new .csv file for relevant transactions.

.. code-block:: python

   python housing.py ../data/pp-complete.csv ../data/london_postcodes.csv ../data/little_csv.csv

A second example of how to extract transactions of interest is in housing_flats.py. It's a short, hacky script that gets properties of type "F" (flats) from north london postcodes in the big .csv file and saves them into a smaller one.

Data Analysis
*************

The script in bramshaw_graphs.py takes a small .csv file containing all transactions at a single poscode and plots them. Multiple transactions on the same property are linked, so you can see how individual properties have changed in value.

N17graphs.py takes a second subset of transactions (in some postcodes in north london) and plots the average transaction values and quartiles over time.

Interest_calculation.py will calculate an approximate quarterly change in the interest rate for a set of selected transactions contained in a csv file.

London_graphs.py will plot the average and total transaction values for different postcode regions.


